function initWorkerGroupsChart(chartId , labels , data);
var data = [<?php echo implode(',', $salaryGroups) ?>];
var labels = ['<?php echo implode("','", array_keys($salaryGroups)) ?>'];
var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
 // The type of chart we want to create
 type: 'line',

 // The data for our dataset
 data: {
     labels: labels,
     datasets: [{
         label: "Worker groups",
         backgroundColor: 'rgb(255, 75, 132)',
         borderColor: 'rgb(255, 120, 132)',
         data: data,
     }]
 },

 // Configuration options go here
 options: {}
});
</script>